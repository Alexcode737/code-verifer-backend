import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

// Configuracion del archivo .env
dotenv.config();

// Creamos la aplicacion con express
const app : Express = express();
const port: string | number = process.env.PORT || 8000;    //Treamos el puerto del archivo .env, si no lo halla lo ponemos manualmente

// Definir nuestra primera ruta de la aplicacion
app.get('/', (request:Request, response:Response) => {
    // Se mostrara un saludo cuando se realize esta peticion
    response.send('Bienvenido. Api Restful: express + ts + nodemon + jest + mongo')
})

// Definir nuestra primera ruta de la aplicacion
app.get('/hello', (request:Request, response:Response) => {
    // Se mostrara un saludo cuando se realize esta peticion
    response.send('welcome to Get route: Helo World')
})


// Ejecutamos la aplicacion y escuchamos las peticiones a un puerto concreto
app.listen(port, () => {
    console.log(`Servidor de express: ejecutandose en el puerto http://localhost:${port}`)
})