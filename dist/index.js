"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Configuracion del archivo .env
dotenv_1.default.config();
// Creamos la aplicacion con express
const app = (0, express_1.default)();
const port = process.env.PORT || 8000; //Treamos el puerto del archivo .env, si no lo halla lo ponemos manualmente
// Definir nuestra primera ruta de la aplicacion
app.get('/', (request, response) => {
    // Se mostrara un saludo cuando se realize esta peticion
    response.send('Bienvenido. Api Restful: express + ts + nodemon + jest + mongo');
});
// Definir nuestra primera ruta de la aplicacion
app.get('/hello', (request, response) => {
    // Se mostrara un saludo cuando se realize esta peticion
    response.send('welcome to Get route: Helo World');
});
// Ejecutamos la aplicacion y escuchamos las peticiones a un puerto concreto
app.listen(port, () => {
    console.log(`Servidor de express: ejecutandose en el puerto http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map